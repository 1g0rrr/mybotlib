_user_actions_utc_timestamps = []
from datetime import datetime, timedelta
from telegram import Update, ParseMode
from telegram.ext import CallbackContext

def _is_overheat(max_actions:int, time_gap:int):

    if len(_user_actions_utc_timestamps) < max_actions: return False

    max_saved_time = _user_actions_utc_timestamps[-max_actions]
    max_alowed_time = datetime.utcnow() - timedelta(seconds=time_gap)

    return max_saved_time > max_alowed_time 

def cooldown_check(callback):
    def wrapper(update: Update, context: CallbackContext, *args, **kwargs):
        current_timestamp = datetime.utcnow()
        

        # in 1 second
        if _is_overheat(max_actions=2, time_gap=1): 
            context.bot.send_message(chat_id=update.message.chat_id, text='Too many actions!')
            return False

        # in 10 seconds
        if _is_overheat(max_actions=10, time_gap=10): 
            context.bot.send_message(chat_id=update.message.chat_id, text='Too many actions!')
            return False
        
        # in 1 minute
        if _is_overheat(max_actions=20, time_gap=60): 
            context.bot.send_message(chat_id=update.message.chat_id, text='Too many actions for 1 minute!')
            return False

        # in 1 hour
        if _is_overheat(max_actions=50, time_gap=60 * 60):
            context.bot.send_message(chat_id=update.message.chat_id, text='Too many actions for 1 hour!')
            return False

        # in 24 hours
        max_actions_in_24_hours = 150
        if _is_overheat(max_actions=max_actions_in_24_hours, time_gap=60 * 60 * 24):
            context.bot.send_message(chat_id=update.message.chat_id, text='Too many actions for 24 hours!')
            return False

        if(len(_user_actions_utc_timestamps) > max_actions_in_24_hours):
            _user_actions_utc_timestamps.pop(0)

        _user_actions_utc_timestamps.append(current_timestamp)
    
        return callback(update, context, *args, **kwargs)
    return wrapper
